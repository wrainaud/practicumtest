import React from 'react';
import "../App.css"; 
import Calendar from '../components/Calendar';
import EquipmentForm from '../components/DashboardReportTable';



function Dashboard() {
    return (
        <div id="wholePage">
          <div id="recentReports" style={{background: 'white', height:'200px'}} >
    
    <h3> Recent Reports </h3>
         
  <EquipmentForm></EquipmentForm>
</div>

<section  style={{width: '100%', marginTop:'1%'}}>
<div id="activityLog" style={{background: 'white', height:'300px'}}>

<h3>Activity Log </h3>

</div>
<div id='caldiv'style={{background: 'white', height:'300px' }}>
<div id="cal" >
<Calendar> </Calendar>

</div>
</div>
</section>
</div>
    );
}

export default Dashboard;