import React from 'react';
import "../App.css";
import EquipList from '../components/EquipList';

function Equipment() {
    return (
        <div>
            <EquipList />
        </div>
    )
}

export default Equipment;