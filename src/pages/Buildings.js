import React, { useEffect } from "react";
import "../App.css";
import { useState } from "react";
import Axios from "axios";
// import ReportMA from "../components/Report-MA";

function Buildings() {
  const [buildingName, setBuildingName] = useState("");
  const [buildingAbv, setBuildingAbv] = useState("");

  const [buildingList, setBuildingList] = useState([]);
  const [equipmentList, setEquipmentList] = useState([]);

  useEffect(() => {
    let ignore = false;
    if (!ignore)  getBuildings()
    return () => { ignore = true; }
  },[]);
    

  const liStyle = {
    margin: "10px",
    padding: "10px",
    background: "#717171",
    border: "1px solid #fff",
  };

  const deleteBtn = {
    marginLeft: "10px",
  };

  const info = {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: "25px",
  };

  const submitBtn = {
    width: "300px",
    height: "50px",
    marginTop: "15px",
  };

  const input = {
    width: "300px",
    fontSize: "20px",
    paddingLeft: "10px",
    margin: "5px"
  }

  const addBuilding = () => {
    if (buildingName === "" || buildingAbv === "") {
      console.log("building input(s) cannot be empty");
    } else {
      Axios.post(
        "https://64vl4ag8uh.execute-api.us-east-1.amazonaws.com/prod/building",
        {
          building_name: buildingName.toUpperCase().trim(),
          building_abv: buildingAbv.toUpperCase().trim(),
        }
      ).then(() => {
        setBuildingList([
          ...buildingList,
          {
            building_name: buildingName.toUpperCase().trim(),
            building_abv: buildingAbv.toUpperCase().trim(),
          },
        ]);
        console.log("success");
      });
    }
  };

  const getBuildings = (name) => {
    Axios.get(
      "https://64vl4ag8uh.execute-api.us-east-1.amazonaws.com/prod/building"
    )
      .then((response) => {
        setBuildingList(response.data);
        //console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const deleteEquipment = (name) => {
    Axios.delete(
      "https://64vl4ag8uh.execute-api.us-east-1.amazonaws.com/prod/equipment",
      {
        data: { mu_equipment_tag: name.toUpperCase().trim() },
      }
    )
  };

  const getRoom = () => {
    Axios.get(
      "https://64vl4ag8uh.execute-api.us-east-1.amazonaws.com/prod/room"
    )
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getEquipment = () => {
    Axios.get(
      "https://64vl4ag8uh.execute-api.us-east-1.amazonaws.com/prod/equipment"
    )
      .then((response) => {
        setEquipmentList(response.data);
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getReport = () => {
    Axios.get(
      "https://64vl4ag8uh.execute-api.us-east-1.amazonaws.com/prod/report"
    )
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <div style={info}>
        <label>Building Name</label>
        <input
          style={input}
          type="text"
          onChange={(event) => {
            setBuildingName(event.target.value);
          }}
        />
        <label>Building Abv</label>
        <input
          style={input}
          type="text"
          onChange={(eventAbv) => {
            setBuildingAbv(eventAbv.target.value);
          }}
        />
        <button style={submitBtn} onClick={addBuilding}>
          Add Building
        </button>
      </div>
      <div>
        <button style={submitBtn} onClick={getEquipment}>
          Show Equipment
        </button>
        <ul>
          {equipmentList.map((val, key) => {
            return (
              <li key={key} style={liStyle}>
                <strong>{val.mu_equipment_tag}</strong>
                <button
                  style={deleteBtn}
                  onClick={() => {
                    deleteEquipment(val.mu_equipment_tag);
                  }}
                >
                  Delete
                </button>
              </li>
            );
          })}
        </ul>
      </div>

      <div>
        <button style={submitBtn} onClick={getReport}>
            Get report in console
        </button>
      </div>

      {/* <select>
        <option>---select---</option>
        {buildingList.map((val, key) => {
              return (
                  <option key={key}>{val.building_name} || {val.building_abv}</option>
              );
            })}
      </select> */}

      {/* <ReportMA /> */}


    </div>
  );
}

export default Buildings;
