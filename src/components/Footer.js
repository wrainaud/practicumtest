import React from 'react';
import './Footer.css';
 


const Footer = () => (

    <div className="footer">
      <div> 
					<a href="https://www.monmouth.edu" target="_blank"> Team Biergarten </a> © Copyright 2021
            <ul class="nav footer-links">
              <li>
                <a style={{margin: 5}} href="https://gitlab.com/mallas/teambiergarten/-/issues">
                  Issues
                </a>
              </li>
              <li>
                <a href="mailto:s0833836@monmouth.edu">
                  Contact
                </a>
              </li>
					  </ul>
      </div>
    </div>
  )
  
  export default Footer;