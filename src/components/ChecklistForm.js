import React from 'react';
import './ChecklistForm.css';
import { useForm } from 'react-hook-form';

const ChecklistForm = () => {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors }
  } = useForm();
  
  const onSubmit = async (data) => {
    const { name, subject, message } = data;
    
    console.log('Name: ', name); 
    console.log('Subject: ', subject);
    console.log('Message: ', message);
  };

  return (
    
    <div className='checklistForm'>
      <div className='container'>
        <div className='row'>
          <div className='col-12 text-center'>
            <div className='checklistForm'>
              <form id='checklist-form' onSubmit={handleSubmit(onSubmit)} noValidate>
                {/* Row 1 of form */}
                <div className='row formRow'>
                  <div className='col-6'>
                    <input
                      type='text'
                      name='name'
                      {...register('name', {
                        required: { value: true, message: 'Please enter Building name' },
                        maxLength: {
                          value: 30,
                          message: 'Please use 30 characters or less'
                        }
                      })}
                      className='form-control formInput'
                      placeholder='Building Name'
                    ></input>
                    {errors.name && <span className='errorMessage'>{errors.name.message}</span>}
                  </div>
                 
                </div>
                {/* Row 2 of form */}
                <div className='row formRow'>
                  <div className='col'>
                    <input
                      type='text'
                      name='room'
                      {...register('room', {
                        required: { value: true, message: 'Please enter Room Number' },
                        maxLength: {
                          value: 75,
                          message: 'Subject cannot exceed 75 characters'
                        }
                      })}
                      className='form-control formInput'
                      placeholder='Room #'
                    ></input>
                    {errors.subject && (
                      <span className='errorMessage'>{errors.subject.message}</span>
                    )}
                  </div>
                </div>
                {/* Row 3 of form */}
                <div className='row formRow'>
                  <div className='col'>
                    <textarea
                      rows={3}
                      name='Comments'
                      {...register('comments', {
                        required: true
                      })}
                      className='form-control formInput'
                      placeholder='Comments'
                    ></textarea>
                    {errors.message && <span className='errorMessage'>Please enter comments</span>}
                  </div>
                </div>
                <button className='submit-btn' type='submit'>
                  Submit
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ChecklistForm;