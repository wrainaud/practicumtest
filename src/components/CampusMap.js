import React from 'react';
import {Jumbotron, Button, Container} from 'react-bootstrap';
import MonmouthCampusMap from '../images/MonmouthCampusMap.jpg'
  
const CampusMap = () => {
    return (
        <div>
            <Jumbotron>
                <Container>
                <h1>Campus Map</h1>
                <p>
                    <a href='./CampusMap'>
                        <img
                            src={MonmouthCampusMap}
                            style={{
                            width: 800,
                            paddingTop: 10
                        }}
                        alt ="#"/>
                    </a>
                </p>

                <p>
                <Button variant="primary">
                    Useless Button
                </Button>
                </p>
                </Container>
            </Jumbotron>
        </div>
  );
}

export default CampusMap;