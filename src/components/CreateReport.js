import React, {useEffect, useState} from "react";
import axios from "axios";
 
import { withRouter } from "react-router-dom";

function CreateReport(props) {
  const [buildingList, setBuildingList] = useState([]);
  const [roomList, setRoomList] = useState([]);
  useEffect(() => {
    let ignore = false;
    if (!ignore)  getBuildings(),getRoom()
    return () => { ignore = true; }
  },[]);
  const onSave = e => {
    let buildingname = e.target[0].value;
    let roomnumber = e.target[1].value;
    let friendlyname = e.target[2].value;
    let status = e.target[3].value;
    let notes = e.target[4].value;

    let data = {

      buildingname,
      roomnumber,
      friendlyname,  
      status,
      notes,
      
    };
    CreateReport(data);
  };
  const getRoom = () => {
    axios.get(
      "https://64vl4ag8uh.execute-api.us-east-1.amazonaws.com/prod/room"
    )
      .then((response) => {
        setRoomList(response.data);
        //console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const getBuildings = () => {
    axios.get(
      "https://64vl4ag8uh.execute-api.us-east-1.amazonaws.com/prod/building"
    )
      .then((response) => {
        setBuildingList(response.data);
        //console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const CreateReport = data => {
    axios
      .post("http://localhost:8080/equipment", data)
      .then(d => {
        console.log(d);
        props.history.push("/");
      })
      .catch(er => alert(er));
  };
  return (
    <div className="container my-3">
      <form
        onSubmit={e => {
          e.preventDefault();
          onSave(e);
        }}
      >
         
          <h3>Create a Report</h3>
          <p>To make a report, fill out the equipment check below and click submit</p> 

       
          <div className="form-group">
          <label>Building Name</label>
          <select>
        <option>---select---</option>
        {buildingList.map((val, key) => {
              return (
                  <option key={key}>{val.building_name} || {val.building_abv}</option>
              );
            })}
      </select>
        </div>
        <div className="form-group">
          <label>Room Number</label>
          <select>
        <option>---select---</option>
        {roomList.map((val, key) => {
          if(setBuildingList === roomList)

              return (
                  <option key={key}>{val.room_name} || {val.room_number}</option>
              );
            })}
      </select>
        </div>
        <div className="form-group">
          <label>Friendly Name</label>
          <input type="text" className="form-control" required />
        </div>
        <div className="form-group">
          <label>Status</label>
          <input type="checkbox" className="form-control" required />
        </div>
        <div className="form-group">
          <label>Notes</label>
          <input type="text" className="form-control" required />
        </div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </div>
  );
}

export default withRouter(CreateReport);