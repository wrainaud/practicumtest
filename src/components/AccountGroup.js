import React from 'react';
import {ButtonGroup, DropdownButton, Dropdown} from 'react-bootstrap';
import './AccountGroup.css';

const AccountGroup = () => {
return(
    <ButtonGroup vertical>
        <DropdownButton as={ButtonGroup} title="John Doe" id="bg-vertical-dropdown-1" variant="light">
            <Dropdown.Item eventKey="1">Notifications</Dropdown.Item>
                <Dropdown.Item 
                    eventKey="2" 
                    href="https://federation.monmouth.edu/adfs/portal/updatepassword" 
                    target="_blank"
                >
                    Change Password
                </Dropdown.Item>
            <Dropdown.Item eventKey="3">Sign Out</Dropdown.Item>
        </DropdownButton>
    </ButtonGroup>
    );
}

export default AccountGroup;