import React, { useState, useEffect } from 'react';
import {Link } from 'react-router-dom';

function EquipmentForm() {
  const [equipformState, setEquipmentFormState] = useState([]);

  useEffect(() => {
    let equipformState = [
      { id: 1, buildingname: "Howard", friendlyname: "lab_eof_1", buildingname: "600 Building",roomnumber: "211",  status: "Working", notes: "Fine"}
    ];

    setEquipmentFormState(
      equipformState.map(d => {
        return {
          select: false,
          id: d.id,
          friendlyname: d.friendlyname,
          buildingname: d.buildingname,
          roomnumber: d.roomnumber,
          status: d.status,
          notes: d.notes,
          
        };
      })
    );
  }, []);

  return (
    <div className="container">
    
      <Link to='./EquipmentCheck' >
        <button
          type="button"
          className="btn btn-primary btn-sm float-right my-3"
        >
          Add Report
        </button>
       
      </Link>
      <table className="table table-bordered table-light">
        <thead>
          <tr>
            <th scope="col">
              <input
                type="checkbox"
                onChange={e => {
                  let checked = e.target.checked;
                  setEquipmentFormState(
                    equipformState.map(d => {
                      d.select = checked;
                      return d;
                    })
                  );
                }}
              ></input>
            </th>
            <th scope="col">Building Name</th>
            <th scope="col">Room Number</th>
            <th scope="col">Friendly Name</th>
            <th scope="col">Status</th>
            <th scope="col">Notes(Optional)</th>

          </tr>
        </thead>
        <tbody>
          {equipformState.map((d, i) => (
            <tr key={d.id}>
              <th scope="row">
                <input
                  onChange={event => {
                    let checked = event.target.checked;
                    setEquipmentFormState(
                      equipformState.map(data => {
                        if (d.id === data.id) {
                          data.select = checked;
                        }
                        return data;
                      })
                    );
                  }}
                  type="checkbox"
                  checked={d.select}
                ></input>
              </th>
              <td>{d.buildingname}</td>
              <td>{d.roomnumber}</td>
              <td>{d.friendlyname}</td>
              <td>{d.status}</td>
              <td>{d.notes}</td>

            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default EquipmentForm;