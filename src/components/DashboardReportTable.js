import React, { useState, useEffect } from 'react';
import {Link } from 'react-router-dom';

function EquipmentForm() {
  const [equipformState, setEquipmentFormState] = useState([]);

  useEffect(() => {
    let equipformState = [
      {  buildingname: "600 Building",roomnumber: "211", equipmenttype: "Projector", status: "Working", notes: "Fine"}
    ];

    setEquipmentFormState(
      equipformState.map(d => {
        return {
          buildingname: d.buildingname,
          roomnumber: d.roomnumber,
          equipmenttype: d.equipmenttype,
          status: d.status,
          notes: d.notes,
          
        };
      })
    );
  }, []);

  return (
    <div className="container">
    
      <table className="table table-bordered table-light">
        <thead>
          <tr>
            <th scope="col">Building Name</th>
            <th scope="col">Room Number</th>
            <th scope="col">Equipment Name</th>
            <th scope="col">Status</th>
            <th scope="col">Notes(Optional)</th>
          </tr>
        </thead>
        <tbody>
          {equipformState.map((d, i) => (
            <tr key={d.id}>
            <td>{d.buildingname}</td>
              <td>{d.roomnumber}</td>
              <td>{d.equipmenttype}</td>
              <td>{d.status}</td>
              <td>{d.notes}</td>
             </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default EquipmentForm;