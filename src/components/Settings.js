import React from 'react';
import {Navbar, Nav, Container,Form,Button, Col, Row, Tab, ListGroup} from 'react-bootstrap';
import './Navigation.css';
 


function Settings() {

    return (
       <div>
            <ul>
                <Nav defaultActiveKey="/Settings" fixed='left'>
  <Nav.Link href='/Settings'>Building and Room Settings</Nav.Link>
  <Nav.Link href="Report Settings">Report Settings</Nav.Link>
    
</Nav>
<h3>Building and Room Settings</h3>
<h5>Create or Remove Any Buildings/ Rooms</h5>

  </ul>
  <Tab.Container id="list-group-tabs-example" defaultActiveKey="#link1">
  <Row>
    <Col sm={4}>
      <ListGroup>
        <ListGroup.Item action href="#link1">
        Howard Hall
        </ListGroup.Item>
        <ListGroup.Item action href="#link2">
        Edison Hall 
        </ListGroup.Item>
        <ListGroup.Item action href="#link3">
        Plangere  
        </ListGroup.Item>
      </ListGroup>
    </Col>
    <Col sm={8}>
      <Tab.Content>
        <Tab.Pane eventKey="#link1">
        </Tab.Pane>
        <Tab.Pane eventKey="#link2">
        </Tab.Pane>
        <Tab.Pane eventKey="#link3">
        </Tab.Pane>
      </Tab.Content>
    </Col>
  </Row>
</Tab.Container>

  </div>

    )
}

export default Settings;