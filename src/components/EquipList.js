import React from 'react';

function EquipList() {
    return (
        <div>
            <p>List of all Equipment here</p>
            <ul>
                <li>Computer</li>
                <li>Phone</li>
            </ul>
        </div>
    )
}

export default EquipList;