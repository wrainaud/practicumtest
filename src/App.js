//Initialization
import React from 'react';
import { Container } from 'react-bootstrap';
import {BrowserRouter as Router, Route} from 'react-router-dom';

//Styles
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

//Components
import Navigation from './components/Navigation';
import CampusMap from './components/CampusMap';
import Footer from './components/Footer';

//Pages
import Dashboard from './pages/Dashboard';
import Buildings from './pages/Buildings';
import Settings from './pages/Settings';
import Equipment from './pages/Equipment';
import Reports from './pages/Reports';
import EquipmentCheck from './pages/EquipmentCheck';
import Add from './pages/Add';

//Main Application Function
function App() {
  return (
    <Container fluid>
      <div className="App">
        <Navigation/>
        <Router>
          <Route exact path='/' component={Dashboard}/>
          <Route exact path='/Dashboard' component={Dashboard}/>
          <Route exact path='/buildings' component={Buildings}/>
          <Route exact path='/equipment' component={Equipment}/>
          <Route exact path='/Reports' component={Reports}/>
          <Route exact path='/CampusMap' component={CampusMap}/>
          <Route exact path='/EquipmentCheck' component={EquipmentCheck}/>
          <Route exact path='/Settings' component={Settings}/>
        </Router>
      </div>
      <Footer/>
    </Container>
  );
}

export default App;
